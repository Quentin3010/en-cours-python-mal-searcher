# MAL Searcher

Quentin BERNARD

## Description du projet

Le projet "MAL Searcher", un programme développé en Python qui facilite la recherche et le téléchargement des miniatures d'animes à partir d'un compte utilisateur sur My Anime List (MAL). 

My Anime List est une plateforme populaire qui permet aux fans d'anime de suivre et de gérer leur collection d'animes (https://myanimelist.net/profile/JeSuisMister).

Ce projet a été conçu dans le but de simplifier la recherche et le filtrage des animes en fonction de différents critères tels que le type d'animé (TV, OAV, etc.), le genre (fantasy, action, etc.) et la date de sortie. 

Ainsi, il est possible de récupérer rapidement les miniatures et de les importés rapidement sur https://tiermaker.com/, afin de faire des tierlists.

Vidéo de présentation : lien

## Fonctionnalités

- Récupération des miniatures des animes associés à un compte utilisateur MAL.
- Filtrage des animes en fonction du type (TV, OAV, etc.), du genre (fantasy, action, etc.) et de la date de sortie.
- Téléchargement sélectif de la première saison des animes choisis.
- Interface conviviale pour faciliter la sélection des filtres.
- Intégration de l'API My Anime List pour accéder aux données et aux informations des animes.