import requests as r
import re
import os
from Season import *
import threading


file_path = os.path.join(os.path.expanduser("~"), "Images") + os.sep + "MAL_Searcher"
images_extension = ".png"
NB_ANIME = 1
animes_ids = []
thread_on = False

def open_folder(path_entry):
    def start_thread():
        print("=>", path_entry.get())
        os.startfile(path_entry.get())
    return start_thread

def get_types(tv, ova, ona, special, movie, music, unknow):
    liste = []
    if(tv.get()):
        liste.append("tv")
    if(ova.get()):
        liste.append("ova")
    if(ona.get()):
        liste.append("ona")
    if(special.get()):
        liste.append("special")
    if(movie.get()):
        liste.append("movie")
    if(music.get()):
        liste.append("music")
    if(unknow.get()):
        liste.append("unknow")
    return liste

def update_progressbar(progressbar, progress, value, nb_dowload):
    global NB_ANIME
    progressbar["value"] = round((value/NB_ANIME)*100)
    progress.config(text="{}% ({}/{}) - {} matches".format(round((value/NB_ANIME)*100), value, NB_ANIME, nb_dowload))

def login(username_entry, progressbar, progress):
    def start_thread():
        global NB_ANIME
        username = username_entry.get()
        #watching, completed, on_hold, dropped, plan_to_watch
        ListStatusEnum = "completed"
        response = r.get("https://api.myanimelist.net/v0/users/" + username + "/animelist?limit=1000&status="+ListStatusEnum)
        
        if response.status_code != 200:
            print(response.status_code)
            username_entry.config(bg="red")
            return
        else:
            username_entry.config(bg="white")
        
        #Get animes
        animes = response.json()["data"]
        animes_ids.clear()
        for node in animes:
            animes_ids.append(node["node"]["id"])
            NB_ANIME = len(animes_ids)
            update_progressbar(progressbar, progress, 0, 0)
    return start_thread
    
def start_thread(username_entry, path_entry, progressbar, progress, genre_matter, liste_deroulante, only_first_season, type_matter, tv, ova, ona, special, movie, music, unknow, start_dates_matter, liste_deroulante_season_debut, liste_deroulante_annee_debut, end_dates_matter, liste_deroulante_season_fin, liste_deroulante_annee_fin):
    def start_thread():
        global thread_on;
        if(thread_on==True):
            return
        thread_on = True
        thread = threading.Thread(target=lambda : download(username_entry, path_entry,
            progressbar, progress, genre_matter.get(), liste_deroulante.get(), 
            only_first_season.get(), type_matter.get(), 
            get_types(tv, ova, ona, special, movie, music, unknow), 
            start_dates_matter.get(), getSeasonEnum(liste_deroulante_season_debut.get()), 
            int(liste_deroulante_annee_debut.get()), 
            end_dates_matter.get(), getSeasonEnum(liste_deroulante_season_fin.get()), 
            int(liste_deroulante_annee_fin.get())))
        
        thread.start()
    return start_thread
    
def download(username_entry, path_entry, progressbar, progress, genres_matter, genre, only_first_season, type_matter, types, start_period_matter, season_start, date_start, end_period_matter, season_end, date_end):
    #on vérifie que l'utilisateur est bien connecté 
    if(len(animes_ids)==0):
        username_entry.config(bg="red")
        return
    else:
        username_entry.config(bg="white")
    
    file_path = path_entry.get();
    #On vide le dossier
    if os.path.exists(file_path):
        for f in os.listdir(file_path):
            os.remove(os.path.join(file_path, f))
    else:
        os.makedirs(file_path)

    #On télécharge si ça remplis les critères
    idx = 0
    nb_dowload = 0
    for id in animes_ids:
        response = r.get("https://api.myanimelist.net/v0/anime/" + str(id) + "?fields=media_type,related_anime,genres,start_season,num_episodes")
        if response.status_code == 200:
            details = response.json()
            anime_good_to_add = True
        else:
            print("=> ERREUR")
            anime_good_to_add = False

        #Type
        #print("- media_type :", details["media_type"])
        if(anime_good_to_add & type_matter & (details["media_type"] not in types)):
            anime_good_to_add = False

        #First season
        #print("- relation :", details["related_anime"])
        if(anime_good_to_add & only_first_season):
            for relation in details["related_anime"]:
                if (relation["relation_type"] == "prequel" or relation["relation_type"] == "parent_story") \
                        and relation["node"]["id"] in animes_ids:
                    anime_good_to_add = False

        #Genre
        #print("- genres :", details["genres"])
        if(anime_good_to_add and genres_matter and genre != ""):
            anime_genres = [genre.get("name") for genre in details["genres"]]
            if(genre not in anime_genres):
                anime_good_to_add = False

        #Période début
        start_year_json = details["start_season"]["year"]
        start_season_json = getSeasonEnum(details["start_season"]["season"])
        if(anime_good_to_add and start_period_matter 
           and not(date_start<start_year_json or (date_start==start_year_json and compareSeason(start_season_json, season_start)>=0))):
            anime_good_to_add = False
            
        #Période fin
        if(anime_good_to_add and end_period_matter 
           and not(date_end>start_year_json or (date_end==start_year_json and compareSeason(start_season_json, season_end)<=0))):
            anime_good_to_add = False
        
        #DOWNLOAD
        if(anime_good_to_add):
            nb_dowload += 1
            anime_name = details["title"]
            filename = re.sub(r"[!\"\#$%&'()*+,-./:;<=>?@\[\]^_`{|}~]", "", anime_name)
            filename = re.sub(r" +", "_", filename)
            img_data = r.get(details["main_picture"]["medium"]).content
            path = file_path + os.sep + filename + images_extension
            u = 2
            while(os.path.exists(path)):
                path = file_path + os.sep + filename + str(u) + images_extension
                u = u + 1
            with open(path, 'wb') as handler:
                handler.write(img_data)
        idx=idx+1
        update_progressbar(progressbar, progress, idx, nb_dowload)
    thread_on = False