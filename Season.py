from enum import Enum

class Season(Enum):
    WINTER = 0
    SPRING = 1
    SUMMER = 2
    FALL = 3
    
def getSeasonEnum(name):
    if(name.lower()=='winter'):
        return Season.WINTER
    elif(name.lower()=='spring'):
        return Season.SPRING
    elif(name.lower()=='summer'):
        return Season.SUMMER
    else:
        return Season.FALL
    
def compareSeason(season1, season2):
    if(season1.value<season2.value):
        return -1
    elif(season1.value>season2.value):
        return 1
    else:
        return 0