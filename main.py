import tkinter as tk
from tkinter import ttk
from datetime import datetime
from RequestAndMethod import *
from tkinter import PhotoImage
import os

###############################################################
def main():
    root = tk.Tk()
    root.title("MAL Searcher | by JeSuisMister")
    root.geometry("600x475") #change les dimensions de la fenêtre
    root.resizable(width=False, height=False)

    ######################
    #LIGNE 1 - Login + Path
    frame1 = tk.Frame(root, width=600, height=150)
    frame1.grid(row=0, column=1, columnspan=2)

    label = tk.Label(frame1, text="Enter your MAL username :")
    label.grid(row=1, column=0, sticky="w", columnspan=2)

    username_entry = tk.Entry(frame1, width=50)
    username_entry.grid(row=2, column=0, sticky="w", columnspan=2)

    label = tk.Label(frame1, text="Enter path where you want to register the images :")
    label.grid(row=3, column=0, sticky="w", columnspan=2)

    path_entry = tk.Entry(frame1, width=50)
    path_entry.insert(0, file_path)
    path_entry.grid(row=4, column=0, sticky="w")

    image_path = os.path.dirname(__file__) + os.sep + "image_dossier.png"
    photo = PhotoImage(file=image_path)
    button_folder = tk.Button(frame1, text="", command=open_folder(path_entry))
    button_folder.config(image=photo)
    button_folder.grid(row=4, column=1, sticky="w")

    ######################
    space = tk.Frame(root, width=600, height=20)
    space.grid(row=1, column=1, columnspan=2)
    label = tk.Label(space, text="")
    label.grid(row=2, column=0, sticky="w")

    #LIGNE 2

    #COLONNE 1
    frame2_1 = tk.Frame(root, width=300, height=150)
    frame2_1.grid(row=2, column=1)
    frame2_1.grid_propagate(False)

    type_matter = tk.IntVar() # variable pour stocker l'état de la case cochable
    type_matter_button = tk.Checkbutton(frame2_1, text="Filter on the anime type ?", variable=type_matter)
    type_matter_button.grid(row=0, sticky = "w")

    sousframe2_1 = tk.Frame(frame2_1, width=300, height=150)
    sousframe2_1.grid(row=1)
    sousframe2_1.columnconfigure(0, pad=50)    

    tv = tk.IntVar() # variable pour stocker l'état de la case cochable
    tv_button = tk.Checkbutton(sousframe2_1, text="TV", variable=tv)
    tv_button.grid(row=0, column=0, sticky = "w")

    ova = tk.IntVar() # variable pour stocker l'état de la case cochable
    ova_button = tk.Checkbutton(sousframe2_1, text="ova", variable=ova)
    ova_button.grid(row=1, column=0, sticky = "w")

    ona = tk.IntVar() # variable pour stocker l'état de la case cochable
    ona_button = tk.Checkbutton(sousframe2_1, text="ona", variable=ona)
    ona_button.grid(row=2, column=0, sticky = "w")

    special = tk.IntVar() # variable pour stocker l'état de la case cochable
    special_button = tk.Checkbutton(sousframe2_1, text="special", variable=special)
    special_button.grid(row=3, column=0, sticky = "w")

    movie = tk.IntVar() # variable pour stocker l'état de la case cochable
    movie_button = tk.Checkbutton(sousframe2_1, text="movie", variable=movie)
    movie_button.grid(row=0, column=1, sticky = "w")

    music = tk.IntVar() # variable pour stocker l'état de la case cochable
    music_button = tk.Checkbutton(sousframe2_1, text="music", variable=music)
    music_button.grid(row=1, column=1, sticky = "w")

    unknow = tk.IntVar() # variable pour stocker l'état de la case cochable
    unknow_button = tk.Checkbutton(sousframe2_1, text="unknow", variable=unknow)
    unknow_button.grid(row=2, column=1, sticky = "w")

    #COLONNE 2
    frame2_2 = tk.Frame(root, width=300, height=150)
    frame2_2.grid(row=2, column=2)
    frame2_2.grid_propagate(False)

    current_year = datetime.now().year

    start_dates_matter = tk.IntVar() # variable pour stocker l'état de la case cochable
    start_dates_matter_button = tk.Checkbutton(frame2_2, text="Filter on the start ?", variable=start_dates_matter)
    start_dates_matter_button.grid(row=0, column=0, sticky = "w")

    liste_deroulante_season_debut = ttk.Combobox(frame2_2, state="readonly")
    liste_deroulante_season_debut.grid(row=1, column=0, sticky="w")
    liste_deroulante_season_debut["values"] = ("Winter", "Spring", "Summer", "Fall")
    liste_deroulante_season_debut.set("Winter") 

    liste_deroulante_annee_debut = ttk.Combobox(frame2_2, state="readonly")
    liste_deroulante_annee_debut.grid(row=1, column=1, sticky="w")
    liste_deroulante_annee_debut["values"] = list(reversed(range(1900,current_year+1)))
    liste_deroulante_annee_debut.set(current_year)

    end_dates_matter = tk.IntVar() # variable pour stocker l'état de la case cochable
    end_dates_matter_button = tk.Checkbutton(frame2_2, text="Filter on the end ?", variable=end_dates_matter)
    end_dates_matter_button.grid(row=2, column=0, sticky = "w")

    liste_deroulante_season_fin = ttk.Combobox(frame2_2, state="readonly")
    liste_deroulante_season_fin.grid(row=3, column=0, sticky="w")
    liste_deroulante_season_fin["values"] = ("Winter", "Spring", "Summer", "Fall")
    liste_deroulante_season_fin.set("Winter") 

    liste_deroulante_annee_fin = ttk.Combobox(frame2_2, state="readonly")
    liste_deroulante_annee_fin.grid(row=3, column=1, sticky="w")
    liste_deroulante_annee_fin["values"] = list(reversed(range(1900,current_year+1)))
    liste_deroulante_annee_fin.set(current_year)

    ######################
    #LIGNE 3 - 

    #COLONNE 1
    frame3_1 = tk.Frame(root, width=300, height=80)
    frame3_1.grid(row=3, column=1, sticky = "nsew")
    frame3_1.grid_propagate(False)

    only_first_season = tk.IntVar() # variable pour stocker l'état de la case cochable
    only_first_season_button = tk.Checkbutton(frame3_1, text="Download only the first season ?", variable=only_first_season)
    only_first_season_button.grid(sticky="nw")

    #COLONNE 2
    frame3_2 = tk.Frame(root, width=300, height=80)
    frame3_2.grid(row=3, column=2, sticky = "nsew")
    frame3_2.grid_propagate(False)

    sousframe3_2 = tk.Frame(frame3_2, width=300, height=150)
    sousframe3_2.grid(row=0, column=0, sticky = "nsew")

    genre_matter = tk.IntVar() # variable pour stocker l'état de la case cochable
    genre_matter_button = tk.Checkbutton(sousframe3_2, text="Filter on the anime genre ?", variable=genre_matter)
    genre_matter_button.pack(side='left')

    liste_deroulante = ttk.Combobox(frame3_2, state="readonly")
    liste_deroulante.grid(row=3, column=0, sticky="w")
    liste_deroulante["values"] = ("Action", "Adventure", "Cars", "Comedy", "Dementia", "Demons", "Drama", "Ecchi", "Fantasy", "Game", "Harem",
          "Hentai", "Historical", "Horror", "Josei", "Kids", "Magic", "Martial Arts", "Mecha", "Military", "Music",
          "Mystery", "Parody", "Police", "Psychological", "Romance", "Samurai", "School", "Sci-Fi", "Seinen", "Shoujo",
          "Shoujo Ai", "Shounen", "Shounen Ai", "Slice of Life", "Space", "Sports", "Super Power", "Supernatural",
          "Thriller", "Vampire", "Yaoi", "Yuri")
    liste_deroulante.set("Action") # défini la valeur par défaut sur "Action"

    ######################
    space = tk.Frame(root, width=600, height=20)
    space.grid(row=4, column=1, columnspan=2)
    label = tk.Label(space, text="")
    label.grid(row=2, column=0, sticky="w")

    #LIGNE 4 - Progress Bar
    frame4 = tk.Frame(root, width=600, height=150)
    frame4.grid(row=5, column=1, columnspan=2)

    progressbar = ttk.Progressbar(frame4, orient="horizontal", length=300, mode="determinate", value=0, maximum=100)
    progressbar.grid(row=1, column=0)

    progress = tk.Label(frame4, text="0% (0/0)")
    progress.grid(row=2, column=0)

    #nbAnimeFound = tk.Label(root, text="Nombre d'animé trouvé dans votre liste : 0")
    #nbAnimeFound.pack()

    ###############################

    buttonLogin = tk.Button(frame1, text="Login!", command=login(username_entry, progressbar, progress))
    buttonLogin.grid(row=5, column=0, sticky="s", columnspan=2)

    buttonDowload = tk.Button(frame4, text="Download!", command=start_thread(username_entry, path_entry, progressbar, progress, genre_matter, liste_deroulante, only_first_season, type_matter, tv, ova, ona, special, movie, music, unknow, start_dates_matter, liste_deroulante_season_debut, liste_deroulante_annee_debut, end_dates_matter, liste_deroulante_season_fin, liste_deroulante_annee_fin))
    buttonDowload.grid(row=5, column=0, sticky="s")

    root.mainloop()

if __name__ == '__main__':
    main()