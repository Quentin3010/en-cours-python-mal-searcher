import requests as r
import re
import os
import math
from Season import *

genres_list = {"Action", "Adventure", "Cars", "Comedy", "Dementia", "Demons", "Drama", "Ecchi", "Fantasy", "Game", "Harem",
          "Hentai", "Historical", "Horror", "Josei", "Kids", "Magic", "Martial Arts", "Mecha", "Military", "Music",
          "Mystery", "Parody", "Police", "Psychological", "Romance", "Samurai", "School", "Sci-Fi", "Seinen", "Shoujo",
          "Shoujo Ai", "Shounen", "Shounen Ai", "Slice of Life", "Space", "Sports", "Super Power", "Supernatural",
          "Thriller", "Vampire", "Yaoi", "Yuri"}

###########
# PHASE 1 #
###########

#Créer le path là où les images iront
images_directory = "C:\\Users\\qbern\\OneDrive\\Bureau\\test"
if not os.path.exists(images_directory):
    os.mkdir(images_directory)
images_extension = ".png"

#Identification
#username = input("Please enter your MAL username : ")
username = "JeSuisMister"
#watching, completed, on_hold, dropped, plan_to_watch
ListStatusEnum = "completed"
while True:
    response = r.get("https://api.myanimelist.net/v0/users/" + username + "/animelist?limit=1000&status="+ListStatusEnum)
    print("https://api.myanimelist.net/v0/users/" + username + "/animelist?limit=1000&status="+ListStatusEnum)
    if response.status_code != 200:
        print("Error code : " + str(response.status_code))
        print("This user's profile couldn't be found, please try again")
        username = input("Please enter your MAL username : ")
    else:
        break

###########
# PHASE 2 #
###########

#Type => {"tv", "ova", "ona", "special", "movie", "music", "unknow"}
type_matter = False
type = {"movie"}
#Est-ce qu'on récupère seulement l'anime si c'est la 1ère saison
only_first_season = False
#Est-ce qu'on récupère en fonction du genre
genres_matter = False
genres = {"Comedy"}
#Est-ce qu'on récupère en fonction d'une période donnée et si oui, laquel
start_period_matter = False
season_start = Season.WINTER
date_start = 2022
end_period_matter = False
season_end = Season.FALL
date_end = 2022

###########
# PHASE 3 #
###########

#On vide le dossier
if os.path.exists(images_directory):
    for f in os.listdir(images_directory):
        os.remove(os.path.join(images_directory, f))

#On récupère tous les animes sous forme de liste d'id
animes = response.json()["data"]
animes_ids = []
for node in animes:
    #print(node)
    animes_ids.append(node["node"]["id"])
print("Nombre animes:",len(animes_ids),"\n")

#On télécharge si ça remplis les critères
idx = 1
for node in animes:
    anime=node["node"]
    response = r.get("https://api.myanimelist.net/v0/anime/" + str(anime["id"]) + "?fields=media_type,related_anime,genres,start_season,num_episodes")
    if response.status_code == 200:
        details = response.json()
        anime_good_to_add = True
    else:
        print("=> ERREUR")
        anime_good_to_add = False

    #Type
    #print("- media_type :", details["media_type"])
    if(anime_good_to_add & type_matter & (details["media_type"] not in type)):
        anime_good_to_add = False

    #First season
    #print("- relation :", details["related_anime"])
    if(anime_good_to_add & only_first_season):
        for relation in details["related_anime"]:
            if (relation["relation_type"] == "prequel" or relation["relation_type"] == "parent_story") \
                    and relation["node"]["id"] in animes_ids:
                anime_good_to_add = False

    #Genre
    #print("- genres :", details["genres"])
    if(anime_good_to_add & genres_matter):
        anime_genres = details["genres"]
        nb_genre = len(genres)
        for node in anime_genres:
            if(node["name"] in genres):
                nb_genre=nb_genre-1
        if(nb_genre!=0):
            anime_good_to_add = False

    #Période début
    start_year_json = details["start_season"]["year"]
    start_season_json = getSeasonEnum(details["start_season"]["season"])
    if(anime_good_to_add and start_period_matter 
       and not(date_start<start_year_json or (date_start==start_year_json and compareSeason(start_season_json, season_start)>=0))):
        anime_good_to_add = False
        
    #Période fin
    if(anime_good_to_add and end_period_matter 
       and not(date_end>start_year_json or (date_end==start_year_json and compareSeason(start_season_json, season_end)<=0))):
        anime_good_to_add = False
        
    #DOWNLOAD
    if(anime_good_to_add):
        anime_name = anime["title"]
        filename = re.sub(r"[!\"\#$%&'()*+,-./:;<=>?@\[\]^_`{|}~]", "", anime_name)
        filename = re.sub(r" +", "_", filename)
        img_data = r.get(anime["main_picture"]["medium"]).content
        path = images_directory + "/" + filename + images_extension
        u = 2
        while(os.path.exists(path)):
            path = images_directory + "/" + filename + str(u) + images_extension
            u = u + 1
        with open(path, 'wb') as handler:
            handler.write(img_data)
        print("=>", u, "saved ", idx, anime_name)
    idx=idx+1

print("\nFINITO")
